using IntegerParser;
using System;
using Xunit;

namespace IntegerParserUnitTests
{
    public class IntegerParserUnitTests
    {
        private readonly IIntegerParser _service;

        public IntegerParserUnitTests()
        {
            _service = new Parser();
        }

        public class Parse:IntegerParserUnitTests
        {
            [Theory]
            [InlineData("12")]
            [InlineData("-15")]
            [InlineData("0")]
            [InlineData("12541")]
            public void Parse_GetValidNumber(string source)
            {
                var expectedResult = int.Parse(source);
                var actualResult = _service.Parse(source);

                Assert.Equal(expectedResult, actualResult);
            }

            [Theory]
            [InlineData("")]
            [InlineData(null)]
            public void Parse_GetArgumentException(string source)
            {
                Assert.Throws<ArgumentException>(() => _service.Parse(source));
            }

            [Theory]
            [InlineData("15.0")]
            [InlineData("12,0")]
            public void Parse_GetFormatException(string source)
            {
                Assert.Throws<FormatException>(() => _service.Parse(source));
            }

            [Theory]
            [InlineData("99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999")]
            public void Parse_GetOverflowException(string source)
            {
                Assert.Throws<OverflowException>(() => _service.Parse(source));
            }
        }
    }
}
