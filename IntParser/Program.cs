﻿using IntegerParser;
using System;

namespace IntParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new Parser();
            while (true)
            {
                Console.WriteLine("Enter number");
                try
                {
                    var number = service.Parse(Console.ReadLine());
                    Console.WriteLine($"You number in int32 = {number}");
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("You enter not correct number. Please enter the correct data");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("The number is too long");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Enter int32 number.");
                }
            }
        }
    }
}