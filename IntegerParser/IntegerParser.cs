﻿using System;

namespace IntegerParser
{
    public class Parser : IIntegerParser
    {
        public int Parse(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException();
            }
            if (source.Contains('.') || source.Contains(','))
            {
                throw new FormatException();
            }

            var result = 0;
            var origin = source;
            var isNegative = false;
            if (source[0] == '-')
            {
                origin = source.Split('-')[1];
                isNegative = true;
            }

            foreach (var symbol in origin)
            {
                result = result * 10 + (symbol - '0');
            }

            if (!isNegative && result < 0)
            {
                throw new OverflowException();
            }

            return isNegative ? -result : result;
        }
    }
}