﻿namespace IntegerParser
{
    public interface IIntegerParser
    {
        int Parse(string source);
    }
}