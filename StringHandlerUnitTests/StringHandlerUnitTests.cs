using ExceptionHandling;
using System;
using Xunit;

namespace StringHandlerUnitTests
{
    public class StringHandlerUnitTests
    {
        private readonly IStringHandler _service;

        public StringHandlerUnitTests()
        {
            _service = new StringHandler();
        }

        public class GetFirstSymbolUnitTests: StringHandlerUnitTests
        {
            [Theory]
            [InlineData(null)]
            [InlineData("")]
            public void GetFirstSymbol_ArgumentException(string source)
            {
                Assert.Throws<ArgumentException>(() => _service.GetFirstSymbol(source));
            }

            [Theory]
            [InlineData("test")]
            [InlineData("try")]
            [InlineData("catch")]
            public void GetFirstSymbol_GetChar(string source)
            {
                var expectedResult = source[0];
                var actualResult = _service.GetFirstSymbol(source);
                Assert.Equal(expectedResult,actualResult);
            }
        }
    }
}
