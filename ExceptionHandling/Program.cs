﻿using System;

namespace ExceptionHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            var handler = new StringHandler();
            while (true)
            {
                Console.WriteLine("Enter string");
                try
                {
                    Console.WriteLine($"First char = {handler.GetFirstSymbol(Console.ReadLine())}");
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("You enter the empty string. Please enter the correct data");
                }
                catch (Exception)
                {
                    Console.WriteLine("Unexpected error.");
                }
            }
        }
    }
}