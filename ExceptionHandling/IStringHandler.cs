﻿namespace ExceptionHandling
{
    public interface IStringHandler
    {
        char GetFirstSymbol(string source);
    }
}