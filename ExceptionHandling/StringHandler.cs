﻿using System;

namespace ExceptionHandling
{
    public class StringHandler : IStringHandler
    {
        public char GetFirstSymbol(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException();
            }
            return source[0];
        }
    }
}